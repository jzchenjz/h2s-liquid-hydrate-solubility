#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This code implements various equations of state. The pressure is in Pa. Sometimes it is difficult to find a good initial guess for DMW, and we use Peng-Robinson for the initial value.
"""
#%%
import numpy as np
import pandas as pd
from scipy.optimize import fsolve
from scipy.integrate import quad

R = 8.3144626
params = {
    'h2s': [9008e3, 373.6, 0.09],
}


def check(P, T):
    """
    Check if the pressure and temperature have compatible sizes.
    """

    # check P, T dimensions and verify consistence
    if isinstance(P, (int, float, list)):
        P = np.array([P], dtype=float).flatten()
    if isinstance(T, (int, float, list)):
        T = np.array([T], dtype=float).flatten()

    if len(P) == 1:
        P = np.tile(P, np.shape(T))
    elif len(T) == 1:
        T = np.tile(T, np.shape(P))
    elif len(P) != len(T):
        raise ValueError(
            'check: P and T must have same dimensions or be singular!')

    return P, T


# ----- EOS ----- #


def pr(P, T, s='h2s'):
    """
    Calculate the compressibility factor using the Peng-Robinson equation of state for vapor and liquid hydrocarbon.
    """

    P, T = check(P, T)
    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()
    Z = np.zeros_like(P)

    species = params.keys()
    s = s.lower()
    if s not in species:
        raise ValueError('pr: No applicable gas!')

    Pc, Tc, ω = params[s]

    Tr = T / Tc
    α = (1 + (0.37464 + 1.54226 * ω - 0.26992 * ω**2) * (1 - np.sqrt(Tr)))**2
    a = 0.45724 * R**2 * Tc**2 / Pc
    b = 0.07780 * R * Tc / Pc
    A = α * a * P / T**2 / R**2
    B = b * P / R / T

    for i, (Ai, Bi) in enumerate(zip(A, B)):

        def fpr(Z, Ai=Ai, Bi=Bi):
            return Z**3 + (Bi - 1) * Z**2 + Z * (Ai - 2 * Bi - 3 * Bi**2) - (
                Ai * Bi - Bi**2 - Bi**3)

        Z[i] = fsolve(fpr, 1)

    return Z.reshape(shape)


def dmw(P, T, s='h2s'):
    """
    Calculate the compressibility factor using the  Duan-Moller-Weare equation of state for CH4, CO2, H2S, NH3, C2H6 and water.
    """

    P, T = check(P, T)

    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()

    Pc, Tc, _ = params[s]

    Pr = P / Pc
    Tr = T / Tc
    Vr = np.zeros_like(Pr)

    # EOS parameters
    # H2S
    a = np.array([
        5.2386075e-2, -2.7463906e-1, -9.6760173e-2, 1.3618104e-2,
        -8.8681753e-2, 4.1176908e-2, 3.6354018e-4, 2.2719194e-3, -7.6962514e-4,
        -2.1948579e-5, -1.1707631e-4, 4.0756926e-5, 5.7582260e-2, 1, 6e-2
    ])

    # function of Z

    def fz(Vr, Tr):
        Zf = 1 + Vr**np.array([[-1, -2, -4, -5]]) @ np.reshape(
            a[:12], [3, 4], order='F').T @ Tr**np.array(
                [[0, -2, -3]]).T + a[12] / Tr**3 / Vr**2 * (
                    a[13] + a[14] / Vr**2) * np.exp(-a[14] / Vr**2)
        return Zf.ravel()

    for i, (Tri, Pri) in enumerate(zip(Tr, Pr)):

        if Pri > 0:

            def fdmw(V, Tri=Tri, Pri=Pri):
                return fz(V, Tri) * Tri - V * Pri

            Vr[i] = fsolve(fdmw, pr(P[i], T[i], s) * Tri / Pri)

    Z = Pr * Vr / Tr
    Z[P == 0] = 1

    return Z.reshape(shape)


#%%
def fugacity(P, T, eosfunc=None):
    """
    Calculate the log of the fugacity coefficient ln(φ) of a pure substance at a pressure P and temperature T given an EOS.
    """

    P, T = check(P, T)
    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()
    lnφ = np.zeros_like(P)

    if eosfunc is None:
        eosfunc = lambda Px, Tx: dmw(Px, Tx, s='ch4')

    for i, (Pi, Ti) in enumerate(zip(P, T)):
        lnφ[i] = quad(lambda y: (eosfunc(y, Ti) - 1) / y, 0, Pi)[0]
    return lnφ.reshape(shape)

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This class contains methods to calculate the pure water properties, such as viscosity and saturation pressure of water vapor.
"""
import numpy as np

Pc = 22.064e6  # critical pressure of water vapor
Tc = 647.14  # critical temperature of water vapor
pa = 101325  # atmospheric pressure
T0 = 273.15  # freezing point
Mw = 0.018015  # water molar mass


def psat(T):
    """
    Calculate the saturation pressure of water vapor using Wagner and Pruss (1993). This does not deviate from the Antoine equation with A = 5.11564, B = 1687.537, C = 230.17.
    """

    if isinstance(T, (int, float, list)):
        T = np.array([T]).flatten()

    # critical temperature and pressure of water
    Tr = T / Tc
    t = 1 - Tr
    # Wagner and Pruss (1993) eq. 1
    a1 = -7.85951783
    a2 = 1.84408259
    a3 = -11.7866497
    a4 = 22.6807411
    a5 = -15.9618719
    a6 = 1.80122502

    return Pc * np.exp(1 / Tr * (a1 * t + a2 * t**1.5 + a3 * t**3 +
                                 a4 * t**3.5 + a5 * t**4 + a6 * t**7.5))


def vm(P, T):
    """
    Calculate the molar volume of liquid water, using the density of saturated water according to Klauda and Sandler (2000).
    The result is slightly smaller than the fit using Wagner and Pruss (1993).
    """

    # eq. 21
    a1 = -10.9241
    a2 = 2.5e-4
    a3 = -3.532e-4
    a4 = 1.559e-7

    t = T - T0
    p = (P - pa) / 1e6

    return np.exp(a1 + a2 * t + a3 * p + a4 * p**2)


def density(T):
    """
    Calculate the density of pure water using Wagner and Pruss (1993).
    """
    # Wagner and Pruss (1993)
    # critical density of water
    ρc = 322
    Tr = T / Tc
    t = (1 - Tr)**(1 / 3)

    # eq. 2
    b1 = 1.99274064
    b2 = 1.09965342
    b3 = -0.510839303
    b4 = -1.75493479
    b5 = -45.5170352
    b6 = -6.7469445e5
    return ρc * (1 + b1 * t + b2 * t**2 + b3 * t**5 + b4 * t**16 + b5 * t**43 +
                 b6 * t**110)


def compressibility(T):
    """
    Calculate the isothermal compressibility of water at a given temperature T using the correlation by Kell (1975), in Pa⁻¹.
    """
    t = T - 273.15
    return np.polyval([
        410.4110e-12, -58.47727e-9, 20.08438e-6, 1.459187e-3, 0.6163813,
        50.88496
    ], t) / (1 + 19.67348e-3 * t) / 1e11

#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate the phase stability of hydrogen sulfide hydrates.

@author: Jiangzhi
"""
#%%
import numpy as np
import pandas as pd
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import h2s
try:
    from palettable.mycarta import LinearL_20 as cmap
    cmap = cmap.mpl_colormap
except ImportError:
    import matplotlib.cm as cm
    cmap = cm.coolwarm

plt.rcParams['figure.dpi'] = 300
plt.style.use(['science', 'nature', 'no-latex'])

R = 8.3144626


def read_solubility():
    """
    Read the data from solubility_h2s.xlsx.
    """
    file = 'data/solubility_h2s.xlsx'
    data = pd.read_excel(file)
    T = data['T (K)'].values
    P = data['P (MPa)'].values
    X = data['m (mol/kg)'].values
    G = data['Phase'].values
    u = data['u (mol/kg)'].values

    return P, T, X, G, u


def fit(T):
    """
    Calculate the H2S hydrate dissociation pressure at a given temperature using Duan et al. (2007) correlations.
    """
    if isinstance(T, (int, float, list)):
        T = np.array([T], dtype=float).flatten()
    Pf = np.zeros_like(T)
    ind = T < 302.7
    Pf[ind] = np.exp(-54.05881 + 0.1492942 * T[ind] + 3624.257 / T[ind])
    Pf[~ind] = 400391.1 - 2754.777 * T[~ind] + 4.731154 * T[~ind]**2
    return Pf * 1e5


#%%
def sol_fit():
    """
    Fit the three-phase and two-phase solubility of H₂S as a function of the temperature. For the three-phase fit, the R² value is calculated.
    """

    P, T, X, G, *_ = read_solubility()

    ind = (G == 'H-Lw-V')
    T3 = T[ind]
    X3 = X[ind]
    # fit X3 to exp(A+B/T3)
    p = np.polyfit(1 / T3, np.log(X3), 1)
    # calculate the r-squared value of the fit
    r2 = 1 - np.sum((np.log(X3) - np.polyval(p, 1 / T3))**2) / np.sum(
        (np.log(X3) - np.mean(np.log(X3)))**2)
    print(f'R² of the three-phase fit: {r2:.4f}')  # 0.9975

    ind = (G == 'H-Lw')
    P2 = P[ind]
    T2 = T[ind]
    X2 = X[ind]

    # fit X2 to exp(A+B/T2+C*P2+D*P2/T2)
    def func(x, a, b, c, d):
        T, P = x
        return np.exp(a + b * P + c / T + d * P / T)

    p0 = 5, 0.1, -6000, 1
    popt, *_ = curve_fit(func, (T2, P2), X2, p0)

    # plot
    plt.figure(figsize=(5, 4))
    ug, ugc = np.unique(P2, return_inverse=True)
    colors = cmap(np.linspace(0, 1, len(ug) + 5))
    Tx = np.linspace(min(T3), max(T3))

    plt.plot(T3,
             np.log(X3),
             'o',
             markersize=3,
             color=colors[0],
             label='H-Lw-V*')
    plt.plot(Tx, np.polyval(p, 1 / Tx), color=colors[0])
    # plot the two-phase solubility as a function of temperature for each pressure
    for i, ugi in enumerate(ug):
        Ti = T2[ugc == i]
        Xi = X2[ugc == i]
        plt.plot(Ti,
                 np.log(Xi),
                 'o',
                 markersize=3,
                 color=colors[i + 1],
                 label=f'{ugi:.0f} MPa')

        plt.plot(Tx, np.log(func((Tx, ugi), *popt)), color=colors[i + 1])

    plt.ylim(top=0.8)
    plt.annotate(
        rf'$\ln m^\ast_\mathrm{{H_2S}} ={{{p[0]:.4f}}}/T+{{{p[1]:.4f}}}$'
        '\n'
        rf'$R^2 = {{{r2:.4f}}}$',
        xy=(280, 0.4),
        fontsize=10,
        horizontalalignment='left',
        verticalalignment='center')

    plt.annotate(
        rf'$\ln m_\mathrm{{H_2S}} ={{{popt[0]:.1f}}}-{{{-popt[1]:.4f}}}P-({{{-popt[2]:.2f}}}-{{{popt[3]:.2f}}}P)/T$',
        xy=(276, -2.15),
        fontsize=10,
        horizontalalignment='left',
        verticalalignment='center')
    plt.xlabel('Temperature (K)')
    plt.ylabel(r'$\ln m_\mathrm{H_2S}$'
               '\n(mol/kg)',
               rotation='horizontal',
               labelpad=15)
    plt.legend(loc='best', frameon=False)
    # calculate the r-squared value of the fit
    R2 = 1 - np.sum((X2 - func((T2, P2), *popt))**2) / np.sum(
        (X2 - np.mean(X2))**2)
    print(f'R² of the three-phase fit: {R2:.4f}')  # 0.9976
    # plt.show()
    plt.savefig('h2s_fit.pdf', bbox_inches='tight')


def plot_heat():
    """
    Plot the ln x ~ 1 / T curve for H - Lw equilibrium under constant pressure, and fit the curve to obtain the reaction heat.
    """
    P, T, X, G, *_ = read_solubility()

    ind = (G == 'H-Lw')
    Ts = T[ind]
    Ps = P[ind]
    Xs = X[ind]

    ug, ugc = np.unique(Ps, return_inverse=True)
    colors = cmap(np.linspace(0, 1, len(ug) + 1))
    H = np.zeros_like(ug)

    plt.figure(figsize=(5, 4))
    for i, ugi in enumerate(ug):
        Ti = Ts[ugc == i]
        Xi = Xs[ugc == i]

        if len(Ti) > 4:
            p = np.polyfit(1 / Ti, np.log(Xi), 1)
            H[i] = -p[0] * R / 1000
            plt.plot(1000 / Ti, np.log(Xi), 'x', markersize=2, color=colors[i])
            plt.plot(1000 / Ti,
                     np.polyval(p, 1 / Ti),
                     lw=1.5,
                     color=colors[i],
                     label=rf'{ugi:.0f} MPa, $\Delta H$ = {H[i]:.2f} kJ/mol')
    plt.ylim(top=0.4)
    plt.legend(loc='best', frameon=False)
    plt.xlabel(r'$1000/T$ (K)')
    plt.ylabel(r'$\ln m_\mathrm{H_2S}$'
               '\n(mol/kg)',
               rotation='horizontal',
               labelpad=8.5)

    # plt.show()
    plt.savefig('h2s_enthalpy.pdf', bbox_inches='tight')


#%%
def plot_lhe():
    """
    Plot the solubility of hydrogen sulfide in Lw-H equilibrium.
    """

    P, T, X, G, *_ = read_solubility()
    ind = G == 'H-Lw-V'  # three-phase
    P3 = P[ind]
    T3 = T[ind]
    X3 = X[G == 'H-Lw-V']

    _, ax = plt.subplots(4, 3, figsize=(6, 8), sharex=True, sharey=True)
    m = h2s.sol(P3 * 1e6, T3)
    plt.subplot(4, 3, 1)
    plt.plot(T3, X3, marker='.', linestyle='', label='data')
    plt.plot(T3, m, label='model')
    plt.legend(loc='best', frameon=False)
    plt.title('H-Lw-V')

    ind = G == 'H-Lw'  # H-Lw two-phase
    Ps = P[ind]
    Ts = T[ind]
    Xs = X[ind]
    ug, ugc = np.unique(Ps, return_inverse=True)

    for i in range(len(ug)):
        Ti = Ts[ugc == i]
        Xi = Xs[ugc == i]
        mi = h2s.sol(ug[i] * 1e6, Ti)
        plt.subplot(4, 3, i + 2)
        plt.plot(Ti, Xi, marker='.', linestyle='', label='data')
        plt.plot(Ti, mi, label='model')
        plt.title(rf'H-Lw, $P$={ug[i]:.0f} MPa')
        plt.legend(loc='upper left', frameon=False)

    for i in [0, 3, 6, 9]:
        ax[i // 3, i % 3].set_ylabel(r"$m_\mathrm{H_2S}$"
                                     "\n(mol/kg)",
                                     fontsize=10,
                                     rotation='horizontal',
                                     labelpad=20)
    for i in [8, 9, 10]:
        ax[i // 3, i % 3].set_xlabel(r'$T$ (K)', fontsize=10)
    ax[-1, -1].set_visible(False)
    # plt.show()
    plt.savefig('h2s_solubility.pdf', bbox_inches='tight')


#%%
def sol_calc():
    """
    Calculate the solubility of hydrogen sulfide as a function of the temperature and pressure.
    """

    P, T, Xexp, _, u = read_solubility()
    Xcalc = np.zeros_like(Xexp)
    for i, (Pi, Ti) in enumerate(zip(P, T)):
        Xcalc[i] = h2s.sol(Pi * 1e6, Ti)

    # calculate the relative deviation from Xexp
    Xdev = (Xcalc - Xexp) / Xexp * 100

    # write a csv containing T, P, Xcalc, Xdev
    with open('data/h2s_calc.csv', 'w') as f:
        f.write('T,P,Xexp,Xcalc,Xdev,u\n')
        for i in range(len(T)):
            f.write(
                f'{T[i]},{P[i]},{Xexp[i]},{Xcalc[i]:.3f},{Xdev[i]:.2f},{u[i]:.4f}\n'
            )
    print('Finished writing data/h2s_calc.csv')


#%%
def analysis():
    """
    Analyze the uncertainties in measurements and deviations in modeling.
    """
    book = pd.read_csv('data/h2s_calc.csv')
    T = book.iloc[:, 0]
    P = book.iloc[:, 1]
    mexp = book.iloc[:, 2]
    mcal = book.iloc[:, 3]
    devpct = book.iloc[:, 4]
    u = book.iloc[:, 5]

    # remove the data for P < 10
    ind = P >= 10
    T = T[ind]
    P = P[ind]
    mexp = mexp[ind]
    mcal = mcal[ind]
    devpct = devpct[ind]
    u = u[ind]
    dev = mcal - mexp

    Pq = range(10, 101, 10)
    Tq = sorted(T.unique())
    devpct_gT = [devpct[T == t] for t in Tq]
    devpct_gP = [devpct[P == p] for p in Pq]
    dev_gT = [dev[T == t] for t in Tq]
    dev_gP = [dev[P == p] for p in Pq]

    _, axs = plt.subplots(figsize=(8, 6), nrows=2, ncols=2)

    # Plot pressure data
    for i, p in enumerate(Pq):
        idx = P == p
        axs[0, 0].errorbar(P[idx],
                           mexp[idx] * 0,
                           yerr=u[idx],
                           fmt='o',
                           markersize=0.5,
                           capsize=2,
                           capthick=0.5,
                           linewidth=0.5,
                           color='blue',
                           label='uncertainty' if i == 0 else None)
    axs[0, 0].boxplot(dev_gP,
                      positions=Pq,
                      widths=1,
                      flierprops=dict(markersize=3),
                      whiskerprops=dict(linestyle='--', linewidth=0.5),
                      boxprops=dict(color='black', linewidth=0.5))

    # add an entry of boxplot in the legend
    axs[0, 0].plot([], [], color='black', label='deviation')
    axs[0, 0].legend(loc='best')
    Plim = [5, 105]
    axs[0, 0].set_xlim(Plim)
    axs[0, 0].set_ylabel(r'$\Delta m$'
                         '\n(mol/kg)',
                         fontsize=10,
                         rotation='horizontal',
                         labelpad=15)
    axs[0, 0].set_xticks(Pq)
    axs[0, 0].set_xticklabels([])
    axs[0, 0].text(0.5,
                   0.1,
                   '(a)',
                   transform=axs[0, 0].transAxes,
                   fontsize=12,
                   va='top')

    # Plot temperature data
    for i, t in enumerate(Tq):
        idx = (T == t) & (P >= 10)
        axs[0, 1].errorbar(T[idx],
                           mexp[idx] * 0,
                           yerr=u[idx],
                           fmt='o',
                           markersize=0.5,
                           capsize=2,
                           capthick=0.5,
                           linewidth=0.5,
                           color='blue',
                           label='uncertainty' if i == 0 else None)
    axs[0, 1].boxplot(dev_gT,
                      positions=Tq,
                      widths=0.3,
                      flierprops=dict(markersize=3),
                      whiskerprops=dict(linestyle='--', linewidth=0.5),
                      boxprops=dict(color='black', linewidth=0.5))
    axs[0, 1].plot([], [], color='black', label='deviation')
    axs[0, 1].legend(loc='best')
    Tlim = [270.7, 303.5]
    axs[0, 1].set_xlim(Tlim)
    axs[0, 1].set_xticks(Tq)
    axs[0, 1].set_xticklabels([])
    axs[0, 1].set_yticklabels([])
    axs[0, 1].text(0.5,
                   0.1,
                   '(b)',
                   transform=axs[0, 1].transAxes,
                   fontsize=12,
                   va='top')

    axs[1, 0].plot(P, devpct, 'o', markersize=1.5)
    axs[1, 0].violinplot(devpct_gP,
                         positions=Pq,
                         widths=5,
                         showextrema=False,
                         bw_method=0.5)
    axs[1, 0].axhline(0, color='black', linewidth=0.5)
    axs[1, 0].set_xlim(Plim)
    axs[1, 0].set_xlabel('Pressure (MPa)')
    axs[1, 0].set_ylabel(r'$\frac{100\Delta m}{m_\mathrm{exp}}$',
                         fontsize=10,
                         rotation='horizontal',
                         labelpad=15)
    axs[1, 0].set_xticks(Pq)
    axs[1, 0].text(0.5,
                   0.1,
                   '(c)',
                   transform=axs[1, 0].transAxes,
                   fontsize=12,
                   va='top')

    axs[1, 1].plot(T, devpct, 'o', markersize=1.5)
    axs[1, 1].violinplot(devpct_gT,
                         positions=Tq,
                         widths=2,
                         showextrema=False,
                         bw_method=0.5)
    axs[1, 1].axhline(0, color='black', linewidth=0.5)
    axs[1, 1].set_xlim(Tlim)
    axs[1, 1].set_xticks(Tq)
    axs[1, 1].set_xlabel('Temperature (K)')
    axs[1, 1].set_yticklabels([])
    axs[1, 1].text(0.5,
                   0.1,
                   '(d)',
                   transform=axs[1, 1].transAxes,
                   fontsize=12,
                   va='top')

    # remove all minor xticks
    for ax in axs.flatten():
        ax.xaxis.set_minor_locator(ticker.NullLocator())
    # strip spacing between subplots
    plt.subplots_adjust(wspace=0.05, hspace=0.05)
    # plt.show()
    plt.savefig('h2s_dev.pdf', bbox_inches='tight')


# %%

if __name__ == "__main__":
    # Figure 4
    sol_fit()
    # Figure 5
    plot_lhe()
    # Figure 6
    sol_calc()
    analysis()
    # Figure 7
    plot_heat()

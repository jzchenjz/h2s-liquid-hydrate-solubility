#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Hydrate equilibrium simulations using the van der Waals-Platteeuw model. These functions can be used to calculate CH4, CO2 and H2S hydrate. Model based on Henry et al. (1990), Sun and Duan (2005), Duan and Sun (2006), and Sun et al. (2020).
"""
#%%
import numpy as np
from scipy.integrate import quad
import water

R = 8.3144626  # J/mol/K
DelHm = 6009.5  # J/mol
Mw = 0.018015  # water molar mass
NA = 6.02214076e23  # Avogadro constant


def vm(P, T):
    """
    Calculate the molar volume of sI hydrate per mole of water using eq. 10 of Sun and Duan (2005). T is in K and P is in Pa.
    """

    P = P / 1e6
    # convert to MPa

    # eq. 10
    Nw = 46
    a1 = 11.818
    a2 = -9.0871e-5
    a3 = 3.9468e-6
    a4 = -4.7254e-9
    a5 = -8.4133e-4
    a6 = 1.5207e-6
    a7 = -2.20e-9

    return (a1 + a2 * T + a3 * T**2 + a4 * T**3 + a5 * P + a6 * P**2 +
            a7 * P**3)**3 * NA / Nw * 1e-30

    # Sun and Duan (2007) eq. 8
    # P = P - 0.1
    # a1 = 11.82
    # a2 = 2.217e-5
    # a3 = 2.242e-6
    # a4 = -3.5e-3
    # a5 = 7.07e-6
    # return (a1 + a2 * T + a3 * T**2)**3 * NA / Nw * np.exp(a4 * P + a5 * P**1.5)


def enthalpy(T, Tq):
    """
    Calculate the water enthalpy change with temperature T between the empty lattice and the water/ice state. The invariant Q1 temperature is given by Tq, which is different for CH4, CO2 and H2S. The result is T * ∫ (Δh/T²) dT over (Tq, T).
    """

    # Table 4 of Sun and Duan (2005). The values are slightly different from Table 1 of Cao et al. (2001). The parameters are used in Sun et al. (2020). Note the signs are different from Munck et al. (1988)

    if isinstance(T, (int, float)):
        T = np.array([T])

    # T <= Tq
    Δh1 = 1300  # J/mol
    ΔCp1 = -38.12  # J/mol/K
    b1 = 0.141  # J/mol/K²
    # T > Tq
    Δh2 = Δh1 - DelHm  # J/mol
    ΔCp2 = 0.565  # J/mol/K
    b2 = 0.002  # J/mol/K²

    # analytical result
    idx = T <= Tq
    T1 = T[idx]
    T2 = T[~idx]

    h = np.zeros_like(T)
    h[idx] = (T1 - Tq) * (
        2 * Δh1 - 2 * ΔCp1 * Tq + b1 * Tq * (T1 + Tq)
    ) / 2 / Tq + T1 * (ΔCp1 - b1 * Tq) * np.log(T1 / Tq)
    h[~idx] = (T2 - Tq) * (
        2 * Δh2 - 2 * ΔCp2 * Tq + b2 * Tq * (T2 + Tq)
    ) / 2 / Tq + T2 * (ΔCp2 - b2 * Tq) * np.log(T2 / Tq)
    return h


def work(P, T, Tq):
    """
    Calculate the change in chemical potential due to work.
    """

    # check P, T dimensions and verify consistence

    shape = np.shape(P)
    P = P.ravel()
    T = T.ravel()

    dw = np.zeros_like(T)

    for i, Ti in enumerate(T):
        Pi = np.linspace(0, P[i], 100)
        Δv = vm(Pi, Ti) - water.vm(Pi, Ti)
        dw[i] = np.trapz(Δv, Pi)
    return dw.reshape(shape)


def potential(P, T, Tq):
    """
    Calculate the potential change of coexisting phase.
    """

    # eq. 5 of Sun and Duan (2005)
    dμ0 = 1202 * T / Tq
    # enthalpy term
    return dμ0 - enthalpy(T, Tq) + work(P, T, Tq)


def occupancy(C1, C2, f):
    """
    Calculate the occupancy of small and large cavities of SI hydrates and the hydrate number.
    """

    θS = C1 * f / (1 + C1 * f)
    θL = C2 * f / (1 + C2 * f)

    # hydration number
    n = 23 / (3 * θL + θS)  # 46 water molecules

    return θS, θL, n


def activity(T, λ, ξ, ms, mg):
    """
    Calculate the activity of water with NaCl following Duan and Sun (2006). When m = 0, the water activity is approximately 1 - xCH4. The calculated results can be compared with the ZSR model.
    """

    # Appendix A, for NaCl
    def par(T, X):
        return X[0] + X[1] * T + X[2] / T + X[3] * np.log(
            T) + X[4] * T**2 + X[5] * T**3

    # Pitzer-Debye-Hückel parameters for NaCl, pdh.xlsx
    # cation = 'Na'
    # anion = 'Cl'
    # ν = 1
    XAφ = np.array([
        86.6836498, 8.48795942e-2, -1.32731477e3, -17.6460172, -8.8878515e-05,
        4.88096393e-8
    ])
    Xβ0 = np.array([
        7.87239712, -8.3864096e-3, -4.96920671e2, -0.82097256, 1.4413774e-5,
        -8.7820301e-9
    ])
    Xβ1 = np.array([
        8.66915291e2, 0.606166931, -1.70460145e4, -1.67171296e2, -4.8048921e-4,
        1.88503857e-7
    ])
    XCφ = np.array([
        1.70761824, 2.32970177e-3, -1.35583596, -0.387767714, -2.46665619e-6,
        1.2154338e-9
    ])

    Aφ = par(T, XAφ)
    α = 2  # univalent
    β0 = par(T, Xβ0)
    β1 = par(T, Xβ1)
    Cφ = par(T, XCφ)

    I = ms
    Z = 2 * ms

    Bφ = β0 + β1 * np.exp(-α * np.sqrt(I))
    C = Cφ / 2  # because zm = zx = 1

    # eq. 6

    mφ = 2 * ms + mg + 2 * (-Aφ * I**1.5 / (1 + 1.2 * np.sqrt(I)) + ms**2 *
                            (Bφ + Z * C) + ms * mg * λ + ms**2 * mg * ξ)

    return -Mw * mφ


def langmuir72(T, g='CH4'):
    """
    Calculate the temperature-dependent Langmuir constants using single layer cage model.
    """

    # sI cage size and coordination number (Parrish & Prausnitz, 1972)
    r1s = 3.975
    z1s = 20
    r1l = 4.3
    z1l = 24
    k = 1.380648813e7  # Boltzmann constant

    if g.lower() == 'ch4':
        # CH4, Table 5.8, Sloan and Koh (2007)
        a = 0.3834
        σ = 3.14393
        Te = 155.393  # ε/k
    elif g.lower() == 'co2':
        # CO2, Table 5.8, Sloan and Koh (2007)
        a = 0.6805
        σ = 2.97638
        Te = 175.405  # ε/k
    elif g.lower() == 'h2s':
        # H2S, Table 5.8, Sloan and Koh (2007)
        a = 0.36
        σ = 3.146  # modified after Sun et al. (2020)
        Te = 205.65  # ε/k, modified after Sun et al. (2020)

    # scaling
    a = a / σ
    Rs = r1s / σ
    Rl = r1l / σ
    θ = T / Te
    P0 = k * T / 4 / np.pi / σ**3

    # ensure δ function converges at r = 0
    def delta(R, r, N):
        return np.maximum(
            ((1 - r / R - a / R)**(-N) - (1 + r / R - a / R)**(-N)) / N, 0)

    def w(R, r):
        return 1 / R**11 / r * (delta(R, r, 10) +
                                a / R * delta(R, r, 11)) - 1 / R**5 / r * (
                                    delta(R, r, 4) + a / R * delta(R, r, 5))

    def varpi(z, R, r):
        return np.exp(-2 * z * w(R, r) / θ)

    return quad(lambda r: varpi(z1s, Rs, r) * r**2, 0, Rs)[0] / P0, quad(
        lambda r: varpi(z1l, Rl, r) * r**2, 0, Rl)[0] / P0

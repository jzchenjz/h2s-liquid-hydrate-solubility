#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Calculate equilibrium properties in H2S gas-liquid-hydrate system, with possible presence of salt, based on Duan et al. (2007). The accuracy is questionable due to lack of data points. An updated parameterization from Sun et al. (2022) can be used.
"""
#%%
import numpy as np
from scipy.optimize import fsolve
import eos, water, hydrate, h2sphase

Tq = 272.4  # invariant Q1
Mw = 0.018015  # water molar mass
R = 8.3144626  # universal gas constant
Pc = 9.008e6  # critical pressure of H2S
Tc = 373.6  # critical temperature of H2S
H = 73.1e3  # dissociation heat (Sun et al., 2021)
# Table 2, with data from Jiang et al. (2020) and Sun et al. (2022)
Cμ = np.array([
    31.4643725629646, -5.380924189629942e-2, -4910.40232017709,
    3.742491227955714e-5, 154355.947283535, 2.979543418938098e-3,
    -0.743335636361053, -4.615642490279356e-5, 6772.73077159550,
    -3.158467590472480e-4, 2.664735692517871e-4
])
Cλ = np.array([
    8.6044430e-2, 3.2742930e-5, -1.7784342, 1.5291720e-5, 0, 0, 0, 0, 0, 0, 0
])
Cξ = -1.0671321e-2

# ----- Gas-liquid equilibrium ----- #


def pv(T):
    """
    Calculate the partial molar volume of H2S using parameters from O'Connell et al. (1996). P is in Pa. The result is in m³.
    The result agrees well with the experimental measurements such as Ellis & McFadden (1972) and Shock et al. (1989). The results for CH4 and CO2 are quite different from recent correlations.
    """
    ρ = water.density(T)
    κT = water.compressibility(T)
    # Table 1
    a, b = 26.6e-4, 0.93e-4
    # eq. 5
    return Mw / ρ + ρ * κT * R * T * (a + b * (np.exp(0.005 * ρ) - 1))


def phi(P, T):
    """
    Calculate the fugacity coefficient φ for H2S using eq. A6 and the DMW eos of H2S. P is in Pa.
    """

    return eos.fugacity(P, T, eosfunc=lambda Px, Tx: eos.dmw(Px, Tx, 'h2s'))

    # shape = np.shape(P)
    # P = P.ravel()
    # T = T.ravel()

    # Pr = P / Pc
    # Tr = T / Tc

    # Z = eos.dmw(P, T, 'h2s')
    # Vr = Z * Tr / Pr

    # # parameters for H2S, from Table A1 of Duan et al. (2007)
    # a = np.array([
    #     5.2386075e-2, -2.7463906e-1, -9.6760173e-2, 1.3618104e-2,
    #     -8.8681753e-2, 4.1176908e-2, 3.6354018e-4, 2.2719194e-3, -7.6962514e-4,
    #     -2.1948579e-5, -1.1707631e-4, 4.0756926e-5, 5.7582260e-2, 1, 6e-2
    # ])

    # # eq. A4
    # def fphi(Zx, Vx, Tx):
    #     return Zx - 1 - np.log(Zx) + 1 / np.array(
    #         [[1, 2, 4, 5]]) * Vx**np.array([[-1, -2, -4, -5]]) @ np.reshape(
    #             a[:12], [3, 4], order='F').T @ Tx**np.array(
    #                 [[0, -2, -3]]).T + a[12] / 2 / Tx**3 / a[14] * (
    #                     a[13] + 1 -
    #                     (a[13] + 1 + a[14] / Vx**2) * np.exp(-a[14] / Vx**2))

    # lnφ = np.array(list(map(fphi, Z, Vr, Tr))).flatten()
    # return lnφ.reshape(shape)


def par(C, P, T):
    """
    Calculate the parameterization with coefficients C and pressure P and T. P is in bar, and T is in K.
    """

    # eq. A5 from Jiang et al. (2020)
    return C[0] + C[1] * T + C[2] / T + C[3] * T**2 + C[4] / (680 - T)**3 + C[
        5] * P + C[6] * P / (680 - T) + C[7] * P**3 / T**2 + C[8] * np.log(
            P) / T**2 + C[9] * np.log(P) * T + C[10] * P**2 / (680 - T)


def potential(P, T):
    """
    Calculate the standard chemical potential μ of H2S in liquid, in hypothetically ideal solution of unit molality using eq. 7. P is in Pa, and the result is in μ/RT.
    """

    P = P / 1e5  # convert to bar

    # in μ_l0 / R / T
    return par(Cμ, P, T)


def interaction(P, T):
    """
    Calculate the interaction parameters λ_H2S_Na and ξ_H2S_Na_Cl, using eq. 9. P is in Pa.
    """

    P = P / 1e5  # convert to bar

    return par(Cλ, P, T), Cξ


def mf(P, T):
    """
    Calculate the mole fraction of H2S in the gas, using eq. 4 and 5. P is in Pa, and ms is the molality of NaCl.
    """

    Pw = water.psat(T)
    y = 1 - Pw / P

    if np.any(y < 0):
        print('mf: pressure is too low!')
        y[y < 0] = 0  # lower than the vapor pressure

    return y


def vlsol(P, T, ms=0):
    """
    Calculate the V-L solubility of H2S in water under P and T. P is in Pa, ms is the molality of NaCl in mol/kg, and the solubility is in mol/kg.
    """

    if np.any(T < Tq):
        print('vlsol: Temperature is below the lower quadruple point!')

    y = mf(P, T)
    y[T < Tq] = 0  # in ice, no dissolved gas
    lnφ = phi(P, T)
    μ = potential(P, T)
    λ, ξ = interaction(P, T)

    P = P / 1e5  # P in bar
    # eq. 9
    return y * P * np.exp(lnφ - μ - 2 * λ * ms - ξ * ms**2)


# ----- van der Waals-Platteeuw ----- #


def equilibrium(P, T, ms):
    """
    Solve for the equilibrium temperature and pressure of H2S hydrate with salt molality ms.
    """

    nwater = 23  # 46 water molecules
    ν1 = 1 / nwater  # 2 small cavities
    ν2 = 3 / nwater  # 6 large cavities
    C1, C2 = hydrate.langmuir72(T, 'H2S')
    f = P * np.exp(phi(P, T))
    θS, θL, *_ = hydrate.occupancy(C1, C2, f)
    dμ = hydrate.potential(P, T, Tq) / R / T

    mg = vlsol(P, T, ms)
    λ, ξ = interaction(P, T)

    return dμ - hydrate.activity(
        T, λ, ξ, ms, mg) + ν1 * np.log(1 - θS) + ν2 * np.log(1 - θL)


def pressure(Tf, ms=0):
    """
    Calculate the dissociation pressure of H2S hydrate under a given temperature Tf and salt molality ms.
    """
    if isinstance(Tf, (int, float, list)):
        Tf = np.array([Tf], dtype=float).flatten()

    # trial pressure, Duan et al. (2007)
    Pt = h2sphase.fit(Tf)
    Pf = np.zeros_like(Tf)
    for i, T in enumerate(Tf):
        Pf[i] = fsolve(lambda Px, T=T: equilibrium(Px, T, ms), Pt[i])

    return Pf


def temperature(Pf, ms=0):
    """
    Calculate the dissociation temperature of H2S hydrate under a given temperature Tf and salt molality ms. With high NaCl molality, it is difficult to find a good trial guess.
    """

    if isinstance(Pf, (int, float, list)):
        Pf = np.array([Pf], dtype=float).flatten()

    if np.any(Pf < 1e5):
        raise ValueError('temperature: Pressure is too low!')

    Tf = np.zeros_like(Pf)
    for i, Pi in enumerate(Pf):
        # use a fit from Duan et al. (2007)
        Tt = fsolve(lambda T, Pi=Pi: h2sphase.fit(T) - Pi, 273.15)
        Tf[i] = fsolve(lambda Tx, Pi=Pi: equilibrium(Pi, Tx, ms), Tt)

    return Tf


def sol(P, T, ms=0):
    """
    Calculate the concentration of dissolved h2s in water in liquid-hydrate system under given pressure and temperature using eq. 27 of Tishchenko et al. (2005) adapted to H2S hydrate.
    """

    P, T = eos.check(P, T)
    Pf = pressure(T, ms)
    m = np.zeros_like(T)
    idx1 = P <= Pf
    idx2 = P > Pf

    if np.sum(idx1) > 0:
        print('sol: No hydrate forms, using G-L equilibrium.')
        m[idx1] = vlsol(P[idx1], T[idx1], ms)
    if np.sum(idx2) > 0:
        print('sol: Hydrate forms, using H-L equilibrium.')
        Ph = P[idx2]
        Th = T[idx2]

        C1, C2 = np.array(list(map(lambda T: hydrate.langmuir72(T, 'H2S'),
                                   Th))).transpose()
        f = Ph * np.exp(phi(Ph, Th))
        _, _, n = hydrate.occupancy(C1, C2, f)
        Pdis = Pf[idx2]
        pmv = pv(Th)
        Δv = n * hydrate.vm(Pdis, Th) - n * water.vm(Pdis, Th) - pmv
        m[idx2] = vlsol(Pdis, Th, ms) * np.exp(Δv * (Ph - Pdis) / R / Th)
        # m[idx2] = np.exp(-6176.6286 / Th + 20.8318) * np.exp(
        # Δv * (Ph - Pdis) / R / Th)
    return m


#%%
